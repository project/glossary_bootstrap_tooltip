<?php
/**
 * @file
 * Admin functions for glossary bootstrap tooltip module.
 */

/**
 * Admin setting form function
 * @param  $form
 * @param  $form_state
 * @return The form array
 */
function glossary_tooltip_settings_form($form, &$form_state) {
  $vocabs = array();
  
  foreach (taxonomy_get_vocabularies() as $vocab) {
    $vocabs[$vocab->vid] = $vocab->name;
  }
  
  $form['glossary_tooltip_vocabs'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Enabled vocabularies'),
      '#description' => t('Select the vocabularies which you want to apply the bootstrap tooltip to.'),
      '#options' => $vocabs,
      '#default_value' => variable_get('glossary_tooltip_vocabs', array()),
  );
  
  return system_settings_form($form);
}